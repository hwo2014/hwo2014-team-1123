package ai;

import noobbot.Main;
import noobbot.message.SendMsg;
import data.GameInfo;
import data.TrackInfo;
import data.TurboInfo;

public class GreedyAI extends AI{

	double prevTrackPiece = -1;
	int pieceCounter = -1;
	boolean hasSwitched = false;
	
	public GreedyAI(TrackInfo trackInfo, GameInfo gameInfo, Main main, Calculator calc, TurboInfo turboInfo) {
		super(trackInfo, gameInfo, main, calc, turboInfo);
	}
	
	@Override
	public SendMsg getTickMessage() {
		double thisTrackPiece = gameInfo.pieceIndex;
		
		// will be executed on first tick of trackpiece
		if (thisTrackPiece != prevTrackPiece) {
			prevTrackPiece = thisTrackPiece;
			pieceCounter = (pieceCounter + 1) % trackInfo.trackPieces.size();
			
			if (trackInfo.getTrackPiece(pieceCounter).isStraight) {
				if (trackInfo.getTrackPiece(pieceCounter+1).isStraight) {
					if (trackInfo.getTrackPiece(pieceCounter+2).isStraight) {
						setThrottle(1.0);
						System.out.println(pieceCounter + " = Straight Straight Straight");
					} else {
						setThrottle(0.7);
						System.out.println(pieceCounter + " = Straight Straight Corner");
					}
				} else {
					if (trackInfo.getTrackPiece(pieceCounter+2).isStraight) {
						setThrottle(0.75);
						System.out.println(pieceCounter + " = Straight Corner Straight");
					} else {
						setThrottle(0.6);
						System.out.println(pieceCounter + " = Straight Corner Corner");
					}
				}
			} else {
				if (trackInfo.getTrackPiece(pieceCounter+1).isStraight) {
					if (trackInfo.getTrackPiece(pieceCounter+2).isStraight) {
						setThrottle(1.0);
						System.out.println(pieceCounter + " = Corner Straight Straight");
					} else {
						setThrottle(0.65);
						System.out.println(pieceCounter + " = Corner Straight Corner");
					}
				} else {
					if (trackInfo.getTrackPiece(pieceCounter+2).isStraight) {
						setThrottle(0.6);
						System.out.println(pieceCounter + " = Corner Corner Straight");
					} else {
						setThrottle(0.5);
						System.out.println(pieceCounter + " = Corner Corner Corner");
					}
				}
			}
			hasSwitched = false;

			/* To prevent flying out of the track in the next rounds */
			if (thisTrackPiece >= 3 && thisTrackPiece <= 5 && gameInfo.lap>0) {
				System.out.println("reduce throttle in new lap");
				setThrottle(0.0);
			}

			//double pieceLength = trackInfo.getTrackPieceLength(pieceCounter, gameInfo.startLaneIndex);
			//System.out.println("Piece length: "+ pieceLength);
		} 
		
		// will be executed on first tick of trackpiece
		else if (!hasSwitched) {
			hasSwitched = true;
			if (pieceCounter == 1 ||pieceCounter == 15) {
				System.out.println(pieceCounter + " = Switch right");
				return switchRightMessage;
			}
			else if (pieceCounter == 5 || pieceCounter == 30 ) {
				System.out.println(pieceCounter + " = Switch left");
				return switchLeftMessage;
			}
		}
		//System.out.println(gameInfo);
		return throttleMessage;
	}

}
