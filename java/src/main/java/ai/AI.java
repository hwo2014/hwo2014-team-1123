package ai;

import noobbot.Main;
import noobbot.message.SendMsg;
import noobbot.message.Switch;
import noobbot.message.Throttle;
import noobbot.message.Turbo;
import data.GameInfo;
import data.TrackInfo;
import data.TurboInfo;

public abstract class AI {
	// race types
	final int UNDEFINED = 0;
	final int QUALIFYING = 1;
	final int RACE = 2;
	
	public TrackInfo trackInfo;
	public GameInfo gameInfo;
	public Calculator calc;
	public Main main;
	public Throttle throttleMessage = new Throttle(0.0);
	public Switch switchLeftMessage = new Switch("Left");
	public Switch switchRightMessage = new Switch("Right");
	// turbo
	public Turbo turboMessage = new Turbo();
	public boolean turboAvailable = false;
	public TurboInfo turboInfo;
	public int raceType = UNDEFINED;
	
	
	public AI(TrackInfo trackInfo, GameInfo gameInfo, Main main, Calculator calc, TurboInfo turboInfo) {
		this.trackInfo = trackInfo;
		this.gameInfo = gameInfo;
		this.main = main;
		this.calc = calc;
		this.turboInfo = turboInfo;
	}
	
	/** Will be called when the Main receives a turbo message */
	public void notifyTurbo() {
		turboAvailable = true;
	}
	
	/** Use the turbo! */
	public SendMsg useTurbo() {
		turboAvailable = false;
		return turboMessage;
	}

	/**
	 * Will be called when the track information is available (at Race init)
	 */
	public void notifyInit() {
		if (raceType == UNDEFINED) raceType = QUALIFYING;
		else if (raceType == QUALIFYING)  raceType = RACE;
	}

	/**
	 * Will be called on every tick
	 */
	public abstract SendMsg getTickMessage();
	
	/**
	 * Update the throttle
	 */
	protected void setThrottle(double value) {
		throttleMessage.setThrottle(value);
	}
}
