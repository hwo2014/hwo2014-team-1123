package ai;

import noobbot.Main;
import noobbot.message.SendMsg;
import data.GameInfo;
import data.TrackInfo;
import data.TurboInfo;

public class DummyAI extends AI {

	public DummyAI(TrackInfo trackInfo, GameInfo gameInfo, Main main, Calculator calc, TurboInfo turboInfo) {
		super(trackInfo, gameInfo, main, calc, turboInfo);
	}

	@Override
	public SendMsg getTickMessage() {
		setThrottle(0.6);
		return throttleMessage;
	}

}