package ai;

import java.util.ArrayList;
import java.util.TreeMap;

import data.GameInfo;
import data.TrackInfo;
import data.TrackPiece;
import data.TurboInfo;

/** This class should be the main logic class for the AI */
public class Calculator {
	final double INF = 100000000;

	public TrackInfo trackInfo;
	public ArrayList<GameInfo> gameInfoHistory;
	public GameInfo gameInfo, prevGameInfo;
	public TurboInfo turboInfo;
	public double power = 0;
	public double brakepower;
	public final double[] scalarsSpeed = {
			0,
			0.0947,
			-7.9708 * Math.pow(10.0,-4.0),
			3.6942 * Math.pow(10.0, -6.0),
			-9.6919 * Math.pow(10.0, -9.0),
			1.3432 * Math.pow(10.0, -11.0),
			-7.6153 * Math.pow(10.0, -15.0),
	};
	public final double[] scalarsDistance = {
			0,
			0.0473,
			-2.6569 * Math.pow(10.0,-4.0),
			9.2356 * Math.pow(10.0, -7.0),
			-1.9384 * Math.pow(10.0, -9.0),
			2.2386 * Math.pow(10.0, -12.0),
			-1.0879 * Math.pow(10.0, -15.0),
	};
	
	// calculated values
	double targetspeed = 0;
	public TreeMap<Integer,Double> switchRoute;
	public ArrayList<TreeMap<Integer,Double>> maxTrackPieceSpeed;
	public ArrayList<TreeMap<Integer,Double>> maxTrackPieceEntrySpeed;

	public double stoppingEntryIndex = 0;
	
	public Calculator(TrackInfo trackInfo, GameInfo gameInfo, TurboInfo turboInfo) {
		this.trackInfo = trackInfo;
		this.gameInfo = gameInfo;
		this.turboInfo = turboInfo;
		this.gameInfoHistory = new ArrayList<GameInfo>();
	}

	/** should be called on every tick (called by main) */
	public void newTick() {
		// append latest tick to the history
		if (!gameInfoHistory.isEmpty()) prevGameInfo = gameInfoHistory.get(gameInfoHistory.size()-1);
		gameInfoHistory.add(gameInfo.copy());
	}
	
	/** returns the distance per tick */
	public double getCurrentSpeed() {
		return getDeltaDist();
	}
	
	/** returns the distance from the previous gameInfo */
	public double getDeltaDist() {
		if (isFirstTickInTrackPiece()) {
			if (gameInfoHistory.size() == 1) return gameInfo.inPieceDistance;
			// otherwise, use piece length of corner
			return gameInfo.inPieceDistance + 
					(trackInfo.getTrackPieceLength((int) prevGameInfo.pieceIndex, prevGameInfo.startLaneIndex, prevGameInfo.endLaneIndex) + 
					- prevGameInfo.inPieceDistance);
			
		} else {
			return gameInfo.inPieceDistance - prevGameInfo.inPieceDistance;
		}
	}
	
	/** Returns whether this game tick is the first tick in the trackPiece */
	public boolean isFirstTickInTrackPiece() {
		if (gameInfoHistory.size()==1) return true;
		else {
			return prevGameInfo.pieceIndex != gameInfo.pieceIndex;
		}
	}
	
	/** Should be called at initialization */
	public void calculateSwitchRoute() {
		// ignore below:
		// it might be possible that switches are alligned in such a way
		// that an optimal solution for round 2 is not optimal for round 3
		// (or getting the fastest time in general), therefore round 3 is 
		// chosen to be the 'optimal' lap
		
		// create a map of lane indexes for switches with the ideal lane positions
		switchRoute = new TreeMap<Integer,Double>();
		
		int prevSwitch = -1;
		int prevMinIndex = 0;
		double[] segmentLength = new double[trackInfo.lanes.size()];
		
		for (int index=0; index<trackInfo.trackPieces.size(); index++) {
			TrackPiece piece = trackInfo.trackPieces.get(index % trackInfo.trackPieces.size());
			for (int i=0; i<trackInfo.lanes.size(); i++)
				segmentLength[i] += trackInfo.getTrackPieceLength(index,i,i); // TODO: update (switches take more distance)
			if (piece.hasSwitch) {
				// find first switch
				if (prevSwitch == -1) {
					prevSwitch = index;
					for (int i=0; i<trackInfo.lanes.size(); i++)
						segmentLength[i] = 0;
				}
				// find next switch
				else {
					// save fastest segment
					double minDist = segmentLength[prevMinIndex];
					int minIndex = prevMinIndex;
					for (int i=0; i<trackInfo.lanes.size(); i++) {
						if (segmentLength[i] < minDist) {
							minDist = segmentLength[i];
							minIndex = i;
						} 
					}
					switchRoute.put(prevSwitch, (double) minIndex);
					prevMinIndex = (int)minIndex; // stay in the same lane
					
					// set up for next switch
					prevSwitch = index;
					for (int i=0; i<trackInfo.lanes.size(); i++)
						segmentLength[i] = 0;
				}
			}
		}
		// last index
		if (prevSwitch != -1) { // (it could be possible that there aren't any switches)
			// add trackpiece lengths from 0..x (where x is the index of the first switch)
			for (int index=0; index<trackInfo.trackPieces.size(); index++) {
				TrackPiece piece = trackInfo.trackPieces.get(index % trackInfo.trackPieces.size());
				for (int i=0; i<trackInfo.lanes.size(); i++)
					segmentLength[i] += trackInfo.getTrackPieceLength(index,i,i); // TODO: similar
				if (piece.hasSwitch) break;
			}
			double minDist = segmentLength[prevMinIndex];
			int minIndex = prevMinIndex;
			for (int i=0; i<trackInfo.lanes.size(); i++) {
				if (segmentLength[i] < minDist) {
					minDist = segmentLength[i];
					minIndex = i;
				}
			}
			switchRoute.put(prevSwitch, (double) minIndex);
		}
		System.out.println(switchRoute);
	}
	
	/** Will determine the maximum speed that the car may have on entering of a trackpiece,
	 * should also consider how the angle of the car influences this. */
	public void calculateMaxTrackPieceSpeed() {
		double cornerAngle = 0;
		double cornerAngleTotal = 0;
		double cornerDirection = 0;
		maxTrackPieceSpeed = new ArrayList<TreeMap<Integer,Double>>();
		for (int i=0; i<trackInfo.lanes.size(); i++) 
			maxTrackPieceSpeed.add(new TreeMap<Integer,Double>());
		maxTrackPieceEntrySpeed = new ArrayList<TreeMap<Integer,Double>>();
		for (int i=0; i<trackInfo.lanes.size(); i++) 
			maxTrackPieceEntrySpeed.add(new TreeMap<Integer,Double>());
		
		// combine corner pieces to get one general corner?
		for (int i=0; i<trackInfo.trackPieces.size(); i++) {
			TrackPiece trackPiece = trackInfo.trackPieces.get(i);
			// go as fast as we can on a straight, in every lane equally fast
			if (trackPiece.isStraight) {
				cornerAngle = 0;
				cornerAngleTotal = 0;
				cornerDirection = 0;
				for (TreeMap<Integer,Double> laneSpeed : maxTrackPieceSpeed)
					laneSpeed.put(i, INF);
				for (TreeMap<Integer,Double> laneSpeed : maxTrackPieceEntrySpeed)
					laneSpeed.put(i, INF);
			}
			// determine how fast we can go in a corner
			else {
				for (int laneIndex=0; laneIndex<maxTrackPieceSpeed.size(); laneIndex++) {
					double radius = trackInfo.getTrackPieceRadius(trackPiece, laneIndex);
					
					double speed =  6.5018 + 0.0347 * (radius-90.0) - 5.4548 * Math.pow(10.0, -5.0) * Math.pow(radius-90.0, 2.0);

					if ((cornerDirection > 0 && trackPiece.angle < 0) ||
						(cornerDirection < 0 && trackPiece.angle > 0)) {
						cornerAngle = 0;
					}
					cornerDirection = trackPiece.angle;
					cornerAngle += trackPiece.angle;
					cornerAngleTotal += trackPiece.angle;
					//ANANAS
					//if (cornerAngle == cornerAngleTotal &&  Math.abs(cornerAngle) <= 45){
					//	System.out.println("ZOINK");
					//		speed = speed + trackPiece.radius/220;}
					// increase speed at start of corner
					/*if (cornerAngle == cornerAngleTotal &&  Math.abs(cornerAngle) <= 45) {
						double factor = trackPiece.radius / 65.0;
						if (factor < 1.1) factor = 1.1;
						if (factor > 1.5) factor = 1.5;
						speed *= factor;
					}*/

					//double speedFactor = 0.99;
					//double entryFactor = 0.53;
					
					double speedFactor = 0.98;
					double entryFactor = 0.3;
					
					speed *= speedFactor;
					if (speed > power) speed = power;
					maxTrackPieceSpeed.get(laneIndex).put(i, speed);
					double entrySpeed = 4.2188 + 0.0506 * radius - 7.5305 * Math.pow(10.0,-5.0) * radius * radius;
					if (entrySpeed > power) entrySpeed = power;
					//entrySpeed = (entrySpeed + speed) / 2.0;
					entrySpeed = entryFactor*entrySpeed  +  (1-entryFactor)*speed;
					maxTrackPieceEntrySpeed.get(laneIndex).put(i, entrySpeed);
				}
			}
		}
		System.out.println("0 maxTrackPieceSpeed      = " + maxTrackPieceSpeed.get(0).values());
		System.out.println("1 maxTrackPieceSpeed      = " + maxTrackPieceSpeed.get(1).values());
		System.out.println("0 maxTrackPieceEntrySpeed = " + maxTrackPieceEntrySpeed.get(0).values());
		System.out.println("1 maxTrackPieceEntrySpeed = " + maxTrackPieceEntrySpeed.get(1).values());
		
		return;
	}
	
	/** Check if we need to set throttle = 0.0 */
	
	double getLane(double trackPieceIndex) {
		double currentLane = gameInfo.endLaneIndex;
		int numberOfTrackPieces = trackInfo.trackPieces.size();
		for (int index = (int) (gameInfo.pieceIndex + 1); index < trackPieceIndex; index ++) {
			if (switchRoute.containsKey((index) % numberOfTrackPieces)) {
				double nextLaneIndex = switchRoute.get((index) % numberOfTrackPieces);
				if (currentLane > nextLaneIndex) {
					currentLane --;
				}
				else if (currentLane < nextLaneIndex) {
					currentLane ++;
				}
			}
		}
		return currentLane;
	}
	
	
	double distanceUntilTrackPiece(double speed, double trackPieceIndex) {
		double distance = trackInfo.getTrackPieceLength((int) gameInfo.pieceIndex, gameInfo.startLaneIndex, gameInfo.endLaneIndex) 
				- gameInfo.inPieceDistance;
		for (double index = gameInfo.pieceIndex + 1; index < trackPieceIndex; index ++) {
			distance += trackInfo.getTrackPieceLength(
					((int) index % trackInfo.trackPieces.size()), getLane(index), getLane(index));
		}
		return distance - speed;
	}
	
	public boolean needToBrake(double throttle) {
		double nextSpeed = calculateNextSpeed(getCurrentSpeed(), 1, throttle);
		
		// check 10 pieces ahead to see if we need to start braking
		for (int i=1; i<10; i++) {
			int pieceIndexFuture = (int)(gameInfo.pieceIndex + i) % trackInfo.trackPieces.size();
			
			double dist = distanceUntilTrackPiece(nextSpeed, gameInfo.pieceIndex + i);
			double stoppingDist = getStoppingDistance(nextSpeed, 
					maxTrackPieceEntrySpeed.get((int) getLane(pieceIndexFuture)).get(pieceIndexFuture));
			
			if (dist < stoppingDist) {
				
				stoppingEntryIndex = pieceIndexFuture;
				targetspeed = maxTrackPieceEntrySpeed.get((int) getLane(pieceIndexFuture)).get(pieceIndexFuture);
				return true;
			}
		}
		
		return false;
	}
	
	public void calculatePower(double speedPower) {
		power = 50 * speedPower;
		brakepower = power;
		
		
	}
	
	public double calculateNextSpeed(double startspeed, double deltaNumberOfTicks, double throttle) {
		// scalar mult
		double total = 0;
		for (int i=0; i<7; i++) {
			total += scalarsSpeed[i] * Math.pow(deltaNumberOfTicks, i);
		}
		if (throttle == 0)
			return startspeed + ( total * brakepower * (throttle - startspeed / brakepower) ) / 5;
		else
		return startspeed + ( total * power * (throttle - startspeed / power) ) / 5;
	}

	public double getStoppingTicks(double startspeed, double endspeed) {
		
		for (double ticks=0; ticks < 300; ticks ++) {
			double tempspeed = calculateNextSpeed(startspeed,ticks,0.0);
			if (tempspeed < endspeed) return ticks + 1;
		}
		return 300;
	}

	public double getStoppingDistance(double startspeed, double endspeed) {
		double ticks = getStoppingTicks(startspeed, endspeed);
		if (endspeed > startspeed) return -1000;

		double total = 0;
		for (int i=0; i<7; i++) {
			total += scalarsDistance[i] * Math.pow(ticks, i+1);
		}
		return (startspeed * ticks) + ( total * brakepower * ( - startspeed / brakepower) ) / 5;
	}

	public double getMaxTrackPieceSpeed() {
		return maxTrackPieceSpeed.get((int) gameInfo.endLaneIndex).
				get((int) gameInfo.pieceIndex);
	}
	
	
}
