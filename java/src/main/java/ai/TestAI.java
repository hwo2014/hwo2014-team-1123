package ai;


import noobbot.Main;
import noobbot.message.SendMsg;
import data.GameInfo;
import data.TrackInfo;
import data.TurboInfo;

public class TestAI extends AI {

	public boolean PRINT_INFO = false;
	public boolean PRINT_PLOT = false;
	public boolean PRINT_ANGLE = false;
	
	double prevTrackPiece = -1;
	boolean hasSwitched = false;
	int numberOfTrackPieces = 0;
	int gameTick = 0;
	
	boolean isBraking = false;
	double brakespeed = 0;
	int braketick = 0;
	int turbotick = 0;

	// initialization variables
	double speedPower = 0;
	double speedBrake = 0;
	
	double prevThrottle;
	
	// TODO: TURBO!
	// accelereren als je te ver onder snelheid zit
	// langer op maximum angle blijven hangen
	// angle van bocht
	// meerdere bochten achter elkaar
	// tegen andere auto's racen
	
	
	
	public TestAI(TrackInfo trackInfo, GameInfo gameInfo, Main main, Calculator calc, TurboInfo turboInfo) {
		super(trackInfo, gameInfo, main, calc, turboInfo);
	}

	@Override
	public void notifyInit() {
		super.notifyInit();
		numberOfTrackPieces = trackInfo.trackPieces.size();
		prevTrackPiece = -1;
		hasSwitched = false;
		if (raceType == QUALIFYING)
			calc.calculateSwitchRoute();
		//calc.switchRoute = new TreeMap<Integer,Double>();
		setThrottle(1.0);
		prevThrottle = 1.0;
	}
	
	/** Returns a switch message if switching is desired, otherwise returns null,
	 * Note that a switch message should be given BEFORE we encounter the switch */
	public SendMsg getSwitch() {
		if (calc.switchRoute.containsKey((int)(gameInfo.pieceIndex + 1) % numberOfTrackPieces)) {
			double currentLaneIndex = gameInfo.endLaneIndex;
			double nextLaneIndex = calc.switchRoute.get((int)(gameInfo.pieceIndex + 1) % numberOfTrackPieces);
			if (currentLaneIndex > nextLaneIndex) {
				//System.out.println(pieceCounter + " = Switch left");
				return switchLeftMessage;
			}
			else if (currentLaneIndex < nextLaneIndex) {
				//System.out.println(pieceCounter + " = Switch right");
				return switchRightMessage;
			}
		}
		return null;
	}
	
	
	boolean waitForTrackPiece = false;
	double trackPieceToWaitFor = 0;
	double entrySpeed = 0;
	
	/** Will be called in every tick, except in the second tick of a trackpiece in
	 * case the car is switching (can be any other tick of the the trackpiece though)*/
	public SendMsg determineThrottle() {
		double currentSpeed = calc.getCurrentSpeed();
		double trackPieceSpeed = calc.getMaxTrackPieceSpeed();
		double throttle = 0;

		// Initially: take throttle based on the trackPieceSpeed
		if (trackPieceSpeed == calc.INF) { // straight
			throttle = 1.0;
		}
		else { // corner
			if (waitForTrackPiece && gameInfo.pieceIndex != trackPieceToWaitFor) {
				throttle = entrySpeed / calc.power;
				//System.out.println("waiting for " + trackPieceToWaitFor);
			} else if (waitForTrackPiece && gameInfo.pieceIndex == trackPieceToWaitFor) {
				waitForTrackPiece = false;
				throttle = trackPieceSpeed / calc.power;
				//System.out.println("waiting done!");
			} else {
			throttle = trackPieceSpeed / calc.power;
			}
		}
		
		// do we need to break?
		if (!isBraking && calc.needToBrake(throttle)) {
			//System.out.println("Start braking");
			isBraking = true;
			waitForTrackPiece = true;
			trackPieceToWaitFor = calc.stoppingEntryIndex;
			entrySpeed = calc.targetspeed;
			brakespeed = currentSpeed;
			braketick = gameTick;
			//if (PRINT_INFO) System.out.println("Braking started at " + gameTick);
			throttle = 0;
		}
		else if (isBraking) {
			// can we stop braking? (throttle for next tick is determined before)
			if (!calc.needToBrake(throttle)) {
				isBraking = false;
				//System.out.println("End braking");
				//if (PRINT_INFO) System.out.println("Braking stopped at " + gameTick);
			}
			// otherwise: continue braking
			else {
				throttle = 0;
			}
		}
		
		// increase throttle to reach the targetspeed
		if (!isBraking) calc.targetspeed = trackPieceSpeed;
		if(calc.targetspeed != calc.INF && currentSpeed < (calc.targetspeed - calc.power / 50) ){
			throttle = 1.0;
		}
			
		
		if (currentSpeed > trackPieceSpeed) throttle = 0;
				
		// angle 
		
		if (!isBraking && Math.abs(calc.prevGameInfo.angle) > Math.abs(gameInfo.angle)){
			if (!trackInfo.getTrackPiece((int)gameInfo.pieceIndex).isStraight){
				if (trackInfo.getTrackPiece((int)gameInfo.pieceIndex + 1).isStraight){
					throttle = 1;
				}
				else if (trackInfo.getTrackPiece((int)gameInfo.pieceIndex+1).angle/
						(trackInfo.getTrackPiece((int)gameInfo.pieceIndex).angle+0.000001) > 0
						&& trackInfo.getTrackPiece((int)gameInfo.pieceIndex + 2).isStraight){
					throttle = 1;
					//System.out.println("BOING");
				}
			}
		}
			
				
		
		
		
		// don't go too extreme with the angle
		/*if (!isBraking &&  Math.abs(gameInfo.angle) > 25 && 
				Math.abs(calc.prevGameInfo.angle) + 1 < Math.abs(gameInfo.angle)) {
			throttle /= 3.0;
		}*/
		
		
		if (PRINT_PLOT) System.out.print(throttle + "],");
		//if (PRINT_PLOT) System.out.print(trackInfo.trackPieces.get((int)gameInfo.pieceIndex).angle + "],");

		//System.out.println(gameInfo.pieceIndex + " throttle: " + throttle + " speed: " + calc.getCurrentSpeed() + 
		//		" trackpiecespeed: " + trackPieceSpeed);
		prevThrottle = throttle;
		setThrottle(throttle);
		return throttleMessage;
	}
	
	@Override
	public SendMsg getTickMessage() {
		//System.out.println(calc.getCurrentSpeed());
		double thisTrackPiece = gameInfo.pieceIndex;
		gameTick ++;
		if (turbotick > 0){
			turbotick --;
			if (turbotick == 0)
				calc.power = calc.power/turboInfo.turboFactor;
			//System.out.println(calc.power);
		}
		
		
		// Determine variables at start of qualifying
		if (calc.power == 0 && raceType==QUALIFYING) return firstTickMessages();
		
		if (turboAvailable && !trackInfo.getTrackPiece((int)gameInfo.pieceIndex).isStraight
				&& trackInfo.getTrackPiece((int)gameInfo.pieceIndex+1).isStraight
				&& trackInfo.getTrackPiece((int)gameInfo.pieceIndex+2).isStraight
				&& trackInfo.getTrackPiece((int)gameInfo.pieceIndex+3).isStraight){
			calc.power = calc.power*turboInfo.turboFactor;
			turbotick = (int) turboInfo.turboDurationTicks;
			if (PRINT_INFO) System.out.println("Turbo Turbo!");
			if (PRINT_PLOT) System.out.print(throttleMessage.getThrottle() + "],");
			return useTurbo();
			
			/*
			 if (turboAvailable && trackInfo.getTrackPiece((int)gameInfo.pieceIndex).isStraight
				&& trackInfo.getTrackPiece((int)gameInfo.pieceIndex+1).isStraight
				&& trackInfo.getTrackPiece((int)gameInfo.pieceIndex+2).isStraight){
			calc.power = calc.power*turboInfo.turboFactor;
			turbotick = (int) turboInfo.turboDurationTicks;
			if (PRINT_INFO) System.out.println("Turbo Turbo!");
			if (PRINT_PLOT) System.out.print(throttleMessage.getThrottle() + "],");
			return useTurbo();
			*/
		/*
		if (turboAvailable && !isBraking && Math.abs(calc.prevGameInfo.angle) > Math.abs(gameInfo.angle)
				&& trackInfo.getTrackPiece((int)gameInfo.pieceIndex+2).isStraight
				&& trackInfo.getTrackPiece((int)gameInfo.pieceIndex+3).isStraight
				&& trackInfo.getTrackPiece((int)gameInfo.pieceIndex+4).isStraight){
			calc.power = calc.power*turboInfo.turboFactor;
			turbotick = (int) turboInfo.turboDurationTicks;
			if (PRINT_INFO) System.out.println("Turbo Turbo!");
			if (PRINT_PLOT) System.out.print(throttleMessage.getThrottle() + "],");
			return useTurbo();
			*/
		}
	

		if (PRINT_ANGLE && gameTick >= 70) {
			System.out.print(gameInfo.angle);

			if (gameTick == 160){
				System.out.println();
				System.exit(0);
			} else System.out.print(",");
		}
		
		
		
		if (PRINT_PLOT) System.out.print("[");

		if (PRINT_PLOT) System.out.print(calc.getCurrentSpeed() + ",");

		if (PRINT_PLOT) System.out.print(gameInfo.angle + ",");
		
		if (PRINT_PLOT) System.out.print(trackInfo.trackPieces.get((int)gameInfo.pieceIndex).radius + ",");
		
		
		if (PRINT_PLOT) {
			if (gameTick == 600) {
				System.out.println("1.0]");
				System.exit(0);
			}
		}
		
		if (thisTrackPiece != prevTrackPiece) {
			prevTrackPiece = thisTrackPiece;
			if (PRINT_INFO) System.out.println("piece " + gameInfo.pieceIndex + " \t speed: "+calc.getCurrentSpeed());
			hasSwitched = false;
		}
		// will be executed on second tick of trackpiece
		else if (!hasSwitched) {
			hasSwitched = true;
			SendMsg switchMsg = getSwitch();
			if (switchMsg != null) {
				if (PRINT_PLOT) 
					System.out.print(throttleMessage.getThrottle() + "],");
				return switchMsg;
			}
			
		}

		return determineThrottle();
	}

	public SendMsg firstTickMessages() {
		setThrottle(1.0);
		
		if (calc.prevGameInfo != null && gameInfo.inPieceDistance != calc.prevGameInfo.inPieceDistance) {
			calc.calculatePower(calc.getCurrentSpeed());
			System.out.println("power = " + calc.power);
			calc.calculateMaxTrackPieceSpeed();
		}
		
		return throttleMessage;
	}
	
	
	
}
