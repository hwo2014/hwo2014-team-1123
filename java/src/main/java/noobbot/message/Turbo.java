package noobbot.message;

/**
 * Builds object for Turbo message
 */
public class Turbo extends SendMsg {
	private String value;

	public Turbo () {
		this.value = "Turbo! Turbo!";
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "turbo";
	}
}