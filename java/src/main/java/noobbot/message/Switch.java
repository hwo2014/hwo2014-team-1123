package noobbot.message;

/**
 * Builds object for Switch message
 */
public class Switch extends SendMsg {
	private String value;

	public Switch (String value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}