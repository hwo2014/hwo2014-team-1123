package noobbot.message;

/**
 * Builds object for Ping message
 */
public class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}