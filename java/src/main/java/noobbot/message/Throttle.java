package noobbot.message;

/**
 * Builds object for Throttle message
 */
public class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}
	
	public void setThrottle(double value) {
		this.value = value;
	}
	

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}

	public double getThrottle() {
		return value;
	}
}