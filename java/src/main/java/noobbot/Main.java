package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.message.MsgWrapper;
import noobbot.message.Ping;
import noobbot.message.SendMsg;
import ai.*;

import com.google.gson.Gson;

import data.GameInfo;
import data.MyCar;
import data.TrackInfo;
import data.TurboInfo;

public class Main {
	
	static boolean CI = true;
	private AI ai;
	private GameInfo gameInfo;
	private TrackInfo trackInfo;
	private TurboInfo turboInfo;
	private MyCar myCar;
	private int carNr;
	private Calculator calc;
	
	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];
		
		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		
		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
		
		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
		
		/*
		 * Examples of messages:
		 */
		// SendMsg message = new Join(botName, botKey);
		// SendMsg message = new CreateRace(new BotId(botName, botKey), trackName, password, carsCount);
		// SendMsg message = new JoinRace(new BotId(botName, botKey), trackName, password, carsCount);
		// SendMsg message = new JoinRaceNoPass(new BotId(botName, botKey), trackName, carsCount);
		
		System.out.println("Car added: "+botName);
		// only for testing
		// need this for CI tests
		SendMsg message;
		if (CI) message = new Join(botName, botKey);
		else message = new CreateRace(new BotId(botName, botKey), "germany", "rtv", 1);
		new Main(reader, writer, message);
		
		socket.close();
	}
	
	final Gson gson = new Gson();
	private PrintWriter writer;
	
	/**
	 * @param message - Can be either Join, JoinRace, CreateRace, ...
	 */
	public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg message) throws IOException {
		this.writer = writer;
		String line = null;

		// Print message
		// System.out.println(message.toJson());
		send(message);

		// initialize objects
		this.gameInfo = new GameInfo();
		this.trackInfo = new TrackInfo();
		this.turboInfo = new TurboInfo();
		this.calc = new Calculator(trackInfo, gameInfo, turboInfo);
		this.myCar = new MyCar();
		this.ai = new TestAI(trackInfo, gameInfo, this, calc, turboInfo); // Use your AI here
		
		boolean raceStarted = false;
		
		while((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
			// Print JSON message
			//if (CI) System.err.println("in:  "+ line);
			if (msgFromServer.msgType.equals("carPositions")) {
            	gameInfo.parseMsg(msgFromServer, this.carNr);	
            	if (raceStarted) {
					calc.newTick();
	            	send(ai.getTickMessage());
            	}
			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("Joined");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("Race init");
				trackInfo.parseInitMsg(msgFromServer);
				
				// Which car are we?
				boolean carFound = false;
				for(int i=0;i<trackInfo.cars.size() && !carFound;i++)
	        	{
	        		if(trackInfo.cars.get(i).name.equals(myCar.name)
	        				&& trackInfo.cars.get(i).color.equals(myCar.color)) {
	        			this.carNr = i;
	        			carFound = true;
	        		}
	        	}
				ai.notifyInit();
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("Race end");
			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				if (((TestAI)ai).PRINT_INFO) System.out.println("Turbo Available");
				turboInfo.parseMsg(msgFromServer);
				ai.notifyTurbo();
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("Race start");
				raceStarted = true;
				calc.newTick();
            	send(ai.getTickMessage());
			} else if (msgFromServer.msgType.equals("yourCar")) {
				myCar.parseInitMsg(msgFromServer);
				System.out.println("Our car: "+myCar.name+" ("+myCar.color+")");
			} else if (msgFromServer.msgType.equals("crash")) {
				System.out.println("CRASHED");
			} else {
				send(new Ping());
			}
		}
	}

	/**
	 * Translate message to JSON and send to server
	 */
	private void send(final SendMsg msg) {
		//if (CI) System.err.println("out: " + msg.toJson());
		writer.println(msg.toJson());
	    writer.flush();
	}
}



/*********************************
 *** CLASSES FOR JSON MESSAGES ***
 *********************************/

/**
 * Builds object for Join message
 */
class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

/**
 * Builds object for CreateRace message
 */
class CreateRace extends SendMsg {
	public BotId botId;
	public String trackName;
	public String password;
	public int carCount;

	public CreateRace (BotId botId, String trackName, String password, int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;      
	}

	@Override
	protected String msgType() {
		return "createRace";
	}
}

/**
 * Builds object for JoinRace message
 */
class JoinRace extends SendMsg {
	public final BotId botId;
	public final String trackName;
	public final String password;
	public final int carCount;

	public JoinRace (final BotId botId, final String trackName, final String password, final int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	@Override
	protected String msgType() {
		return "joinRace";
	}
}

/**
 * Builds object for JoinRace message for races without a password
 */
class JoinRaceNoPass extends SendMsg {
	public final BotId botId;
	public final String trackName;
	public final int carCount;

	public JoinRaceNoPass (final BotId botId, final String trackName, final int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.carCount = carCount;
	}

	@Override
	protected String msgType() {
		return "joinRace";
	}
}

/**
 * Builds object for the BotId part needed for JoinRace / CreateRace messages
 */
class BotId extends SendMsg {
	public String name;
	public String key;
	
	public BotId (String name, String key) {
		this.name = name;
		this.key = key;
	}
	
	protected String msgType() {
		return "botId";
	}
}