package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.message.SendMsg;

public class Bot {

	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];
		
		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		
		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
		
		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
		
		/*
		 * Examples of messages:
		 */
		// SendMsg message = new Join(botName, botKey);
		// SendMsg message = new CreateRace(new BotId(botName, botKey), trackName, password, carsCount);
		// SendMsg message = new JoinRace(new BotId(botName, botKey), trackName, password, carsCount);
		// SendMsg message = new JoinRaceNoPass(new BotId(botName, botKey), trackName, carsCount);
		
		System.out.println("Car added: "+botName);
		SendMsg message = new JoinRace(new BotId(botName, botKey), "keimola", "rtv", 2);
		new Main(reader, writer, message);
		
		socket.close();
	}
}