package data;

import com.google.gson.internal.LinkedTreeMap;

import noobbot.message.MsgWrapper;

public class TurboInfo {

	public double turboDurationMilliseconds = 0;
	public double turboDurationTicks = 0;
	public double turboFactor = 0;
	
	public void parseMsg(MsgWrapper msg) {
		this.turboDurationMilliseconds = (Double) ((LinkedTreeMap<?, ?>) msg.data).get("turboDurationMilliseconds");
		this.turboDurationTicks = (Double) ((LinkedTreeMap<?, ?>) msg.data).get("turboDurationTicks");
		this.turboFactor = (Double) ((LinkedTreeMap<?, ?>) msg.data).get("turboFactor");
	}
}
