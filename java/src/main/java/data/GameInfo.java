package data;

import java.util.ArrayList;

import noobbot.message.MsgWrapper;

import com.google.gson.internal.LinkedTreeMap;

public class GameInfo {

	public String name;
	public String color;
	public double angle;
	public double pieceIndex;
	public double inPieceDistance;
	public double startLaneIndex;
	public double endLaneIndex;
	public double lap;
	
	
	/** Parses the game info tick */
	public void parseMsg(MsgWrapper msg, int carNr) {
		LinkedTreeMap<?, ?> data =  (LinkedTreeMap<?, ?>) ((ArrayList<?>) msg.data).get(carNr);
    	name = (String) ((LinkedTreeMap<?, ?>) data.get("id")).get("name");
    	color = (String) ((LinkedTreeMap<?, ?>) data.get("id")).get("color");
    	angle = (Double) data.get("angle");
    	pieceIndex = (Double) ((LinkedTreeMap<?, ?>) data.get("piecePosition")).get("pieceIndex");
    	inPieceDistance = (Double) ((LinkedTreeMap<?, ?>) data.get("piecePosition")).get("inPieceDistance");
    	startLaneIndex = (Double) ((LinkedTreeMap<?, ?>) ((LinkedTreeMap<?, ?>) data.get("piecePosition")).get("lane")).get("startLaneIndex");
    	endLaneIndex = (Double) ((LinkedTreeMap<?, ?>) ((LinkedTreeMap<?, ?>) data.get("piecePosition")).get("lane")).get("endLaneIndex");
    	lap = (Double) ((LinkedTreeMap<?, ?>) data.get("piecePosition")).get("lap");
	}
	
	/** returns a copy of this instance */
	public GameInfo copy() {
		GameInfo cp = new GameInfo();
		cp.name = this.name;
		cp.color = this.color;
		cp.angle = this.angle;
		cp.pieceIndex = this.pieceIndex;
		cp.inPieceDistance = this.inPieceDistance;
		cp.startLaneIndex = this.startLaneIndex;
		cp.endLaneIndex = this.endLaneIndex;
		cp.lap = this.lap;
		return cp;
	}
	
	
	public String toString() {
		return "GameInfo: \n"
				+ "\tname: " + name  + "\n"
				+ "\tcolor: " + color  + "\n"
				+ "\tangle: " + angle  + "\n"
				+ "\tpieceIndex: " + pieceIndex  + "\n"
				+ "\tinPieceDistance: " + inPieceDistance  + "\n"
				+ "\tstartLaneIndex: " + startLaneIndex  + "\n"
				+ "\tendLaneIndex: " + endLaneIndex  + "\n"
				+ "\tlap: " + lap  + "\n";
	}
	
	
}
