package data;

public class CarInfo {
	public String name = "";
	public String color = "";
	public double length = 0.0;
	public double width = 0.0;
	public double guideFlagPosition = 0.0;

	public String toString() {
		return "CarInfo: \n"
				+ "\tname: " + name  + "\n"
				+ "\tcolor: " + color  + "\n"
				+ "\tlength: " + length  + "\n"
				+ "\twidth: " + width  + "\n"
				+ "\tguideFlagPosition: " + guideFlagPosition  + "\n";
	}
}