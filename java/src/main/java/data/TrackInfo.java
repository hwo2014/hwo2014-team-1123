package data;

import java.util.ArrayList;
import java.util.TreeMap;

import noobbot.message.MsgWrapper;

import com.google.gson.internal.LinkedTreeMap;

/**
 * This class will parse and store the track information.
 * This consists of all track segments as well as the number
 * of laps and maximum time per lap and cars. This class will
 * also need methods to easily access the individual parts
 * @author Vincent
 *
 */
public class TrackInfo {
	
	// track info
	public ArrayList<TrackPiece> trackPieces;
	public TreeMap<Double,Double> lanes;
	public String trackId;
	public String trackName;
	public double startingPointX;
	public double startingPointY;
	public double startingPointAngle;

	// car info
	public ArrayList<CarInfo> cars;
	
	// raceSession info
	public double laps;
	public double maxLapTimeMs;
	public boolean quickRace;
	
	// methods
	
	/** Initial parsing*/
	@SuppressWarnings("unchecked")
	public void parseInitMsg(MsgWrapper msg) {
		LinkedTreeMap<?, ?> data = (LinkedTreeMap<?, ?>) ((LinkedTreeMap<?, ?>) msg.data).get("race");
		LinkedTreeMap<?, ?> trackData = (LinkedTreeMap<?, ?>) data.get("track");
		ArrayList<?> carsData = (ArrayList<?>) data.get("cars");
		LinkedTreeMap<?, ?> raceSession = (LinkedTreeMap<?, ?>) data.get("raceSession");
		// track
		trackId = (String) trackData.get("id");
		trackName = (String) trackData.get("name");
		trackPieces = new ArrayList<TrackPiece>();
		for (LinkedTreeMap<?, ?> piece : (ArrayList<LinkedTreeMap<?, ?>>) trackData.get("pieces")) {
			// is it a straight?
			TrackPiece trackPiece = new TrackPiece();
			if (piece.containsKey("length")) {
				trackPiece.isStraight = true;
				trackPiece.setLength((Double) piece.get("length"));
			} else { // corner otherwise
				trackPiece.isStraight = false;
				trackPiece.angle = (Double) piece.get("angle");
				trackPiece.radius = (Double) piece.get("radius");
			}
				
			// does it contain a switch?
			if (piece.containsKey("switch")) {
				trackPiece.hasSwitch = (Boolean) piece.get("switch");
			}
			trackPieces.add(trackPiece);
		}
		lanes = new TreeMap<Double,Double>();
		for (LinkedTreeMap<?, ?> lane : (ArrayList<LinkedTreeMap<?,?>>) trackData.get("lanes")) {
			lanes.put((Double)lane.get("index"), (Double)lane.get("distanceFromCenter"));
		}

		// cars
		cars = new ArrayList<CarInfo>();
		for (LinkedTreeMap<?, ?> car : (ArrayList<LinkedTreeMap<?, ?>>) carsData) {
			CarInfo carInfo = new CarInfo();
			carInfo.name = (String) ((LinkedTreeMap<?, ?>) car.get("id")).get("name");
			carInfo.color = (String) ((LinkedTreeMap<?, ?>) car.get("id")).get("color");
			carInfo.length = (Double) ((LinkedTreeMap<?, ?>) car.get("dimensions")).get("length");
			carInfo.width = (Double) ((LinkedTreeMap<?, ?>) car.get("dimensions")).get("width");
			carInfo.guideFlagPosition = (Double) ((LinkedTreeMap<?, ?>) car.get("dimensions")).get("guideFlagPosition");
			cars.add(carInfo);
		}
		
		// raceSession
		if (raceSession.containsKey("laps"))
			laps = (Double) raceSession.get("laps");
		if (raceSession.containsKey("maxLapTimeMs"))
			maxLapTimeMs = (Double) raceSession.get("maxLapTimeMs");
		if (raceSession.containsKey("quickRace"))
			quickRace = (Boolean) raceSession.get("quickRace");
	}
	
	/**
	 * Returns the corresponding trackPiece. Also prevents overflows by taking the modulo
	 */
	public TrackPiece getTrackPiece(int index) {
		if (index < 0) index += trackPieces.size();
		return trackPieces.get(index % trackPieces.size());
	}
	
	/** Returns the length of the trackPiece, also works for corners */
	public double getTrackPieceLength(int trackPieceIndex, double startLaneIndex, double endLaneIndex) {
		TrackPiece piece = trackPieces.get(trackPieceIndex % trackPieces.size());
		
		if (startLaneIndex != endLaneIndex) {
			return switchPieceLength(trackPieceIndex % trackPieces.size(), startLaneIndex, endLaneIndex);
		}
		else if (piece.isStraight)
			return piece.getLength();
		else {
			return 2 * getTrackPieceRadius(piece,startLaneIndex) * Math.PI * (Math.abs(piece.angle)/360);
		}
	}

	/** Should only be called if startLaneIndex != endLaneIndex */
	public double switchPieceLength(double pieceIndex, double startLaneIndex, double endLaneIndex) {
		TrackPiece piece = getTrackPiece((int) pieceIndex);
		if (piece.isStraight) {
			double ydiff = Math.abs(lanes.get(startLaneIndex) - lanes.get(endLaneIndex));
			double xdiff = piece.getLength();
			return Math.sqrt(Math.pow(ydiff, 2.0) + Math.pow(xdiff, 2.0));
		} else {
			/* assuming |angle| < 90 
			double x0 = 0;
			double y0 = getTrackPieceRadius(piece, startLaneIndex);
			double beta = 90 - Math.abs(piece.angle);
			double s = getTrackPieceRadius(piece, endLaneIndex);
			double x1 = s * Math.cos(beta/180.0 * Math.PI);
			double y1 = s * Math.sin(beta/180.0 * Math.PI);*/
			
			double minLane = getTrackPieceLength((int) pieceIndex,endLaneIndex,endLaneIndex);
			double maxLane = getTrackPieceLength((int) pieceIndex,startLaneIndex,startLaneIndex);
			if (minLane > maxLane) {
				minLane = maxLane;
				maxLane = getTrackPieceLength((int) pieceIndex,endLaneIndex,endLaneIndex);
			}
			
			return (0.82*maxLane + 0.18*minLane) ;
		}
	}
	
	public double getTrackPieceRadius(TrackPiece piece, double laneIndex) {
		if (piece.isStraight) return 0;
		double extraDist = 0;
		if (piece.angle < 0) extraDist = lanes.get(laneIndex);
		else extraDist = lanes.get(lanes.size()-laneIndex-1);
		return piece.radius + extraDist;
	}
	
	
	/** 3 points circle */
	
}

