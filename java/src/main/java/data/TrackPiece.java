package data;

public class TrackPiece {
	public boolean isStraight = true;
	private double length = 0.0;
	public double radius = 0.0;
	public double angle = 0.0;
	public boolean hasSwitch = false;
	
	/** For initialization purposes */
	public void setLength(double length) {
		this.length = length;
	}

	public double getLength() {
		return length;
	}
	
	public String toString() {
		return "TrackPiece: \n"
				+ "\tisStraight: " + isStraight  + "\n"
				+ "\tlength: " + length  + "\n"
				+ "\tradius: " + radius  + "\n"
				+ "\tangle: " + angle  + "\n"
				+ "\thasSwitch: " + hasSwitch  + "\n";
	}
}