package data;

import java.util.ArrayList;

import com.google.gson.internal.LinkedTreeMap;

import noobbot.message.MsgWrapper;

public class MyCar {
	public String name = "";
	public String color = "";
	
	public void parseInitMsg(MsgWrapper msg) {
		LinkedTreeMap<?, ?> data = (LinkedTreeMap<?, ?>) ((LinkedTreeMap<?, ?>) msg.data);
		this.name = (String) data.get("name");
		this.color = (String) data.get("color");
	}

	public String toString() {
		return "MyCar: \n"
				+ "\tname: " + name  + "\n"
				+ "\tcolor: " + color  + "\n";
	}
}